# Blog Post with Two Columns and Buy Button - HTML



To create a page \ blog post with two columns follow these steps. A buy button is included as an example. 

[View Example Page\Post Here](https://blogpost-buybutton-demo.myshopify.com/blogs/news/example-post)

1. Add the code in [theme.scss.liquid](https://bitbucket.org/mttcmrn/blog-post-or-page-with-2-columns-and-buy-button-html/src/b03ef9a60c6d433aa17e65c33b96bf2fa91268f1/theme.scss.liquid?at=master) to the existing scss file in the store theme code. Different themes may have different scss names. The default is theme.scss.liquid 
2. Use the example code in [example-with-buy-button](https://bitbucket.org/mttcmrn/blog-post-or-page-with-2-columns-and-buy-button-html/src/b03ef9a60c6d433aa17e65c33b96bf2fa91268f1/example_with_buy_button?at=master) as the starting point for a new page \ post. Paste into the HTML editor. 

![alt](https://screenshot.click/4229-27-11-kempy.jpg)